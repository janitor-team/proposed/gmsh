#!/usr/bin/make -f
BUILDDIR = $(CURDIR)/debian/build

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -Wall -pedantic

include /usr/share/dpkg/architecture.mk

%:
	dh $@ --buildsystem=cmake --builddirectory=$(BUILDDIR)

disable_auto_test_archs = mips64el
export LD_LIBRARY_PATH := $(CURDIR)/debian/tmp/usr/lib:$(LD_LIBRARY_PATH);

# Don't enable PETSc support. We would need to use an MPI compiler (which isn't
# recommended), and all functionality is present without PETSc as well. See
# <https://gitlab.onelab.info/gmsh/gmsh/issues/502#note_6599>.
#
# Turn off MED support for now. All the dependencies are there in Debian
# (libhdf5-serial-dev, libmed-dev, libmedc-dev), but libmed is provided
# MPI-compiled only, leading to errors as described in
# <https://gitlab.onelab.info/gmsh/gmsh/issues/504#note_6602>.
#
# GMSH_HOST and GMSH_PACKAGER are explicitly specified to help with reproducible builds.
extra_flags += \
    -DCMAKE_SKIP_RPATH:BOOL=ON \
    -DENABLE_BUILD_SHARED:BOOL=ON \
    -DENABLE_BUILD_DYNAMIC:BOOL=ON \
    -DGMSH_HOST:STRING="debian-build-farm" \
    -DGMSH_PACKAGER:STRING="nobody" \
    -DENABLE_OPENMP:BOOL=ON \
    -DENABLE_OS_SPECIFIC_INSTALL:BOOL=ON \
    -DENABLE_SYSTEM_CONTRIB:BOOL=ON \
    -DENABLE_CGNS:BOOL=ON \
    -DENABLE_MED:BOOL=OFF \
    -DENABLE_METIS:BOOL=ON \
    -DENABLE_OCC:BOOL=ON \
    -DOCC_LIB:STRING="/usr/lib/${DEB_HOST_MULTIARCH}" \
    -DENABLE_OCC_CAF:BOOL=ON \
    -DENABLE_ONELAB:BOOL=ON \
    -DENABLE_OSMESA:BOOL=OFF \
    -DENABLE_PETSC:BOOL=OFF \
    -DEIGEN_INC:STRING="/usr/include/eigen3" \
    -DENABLE_PRIVATE_API:BOOL=ON

override_dh_auto_clean:
	rm -rf $(BUILDDIR)
	rm -rf $(CURDIR)/debian/tmp
	rm -f doc/texinfo/gmsh.vr
	rm -f doc/texinfo/gmsh.fn
	rm -f doc/texinfo/gmsh.ky
	rm -f doc/texinfo/gmsh.pg
	rm -f doc/texinfo/gmsh.toc
	rm -f doc/texinfo/gmsh.log
	rm -f doc/texinfo/gmsh.cp
	rm -f doc/texinfo/gmsh.tp
	rm -f doc/texinfo/gmsh.tps
	rm -f doc/texinfo/gmsh.cps
	rm -f doc/texinfo/gmsh.aux
	rm -f doc/texinfo/gmsh.html
	rm -f doc/texinfo/gmsh.pdf
	rm -f doc/texinfo/gmsh.info

override_dh_auto_configure:
	dh_auto_configure -- $(extra_flags)

override_dh_auto_build:
	dh_auto_build
	# To avoid the texi2dvi pb add LC_ALL=C
	dh_auto_build -- pdf html info LC_ALL=C

override_dh_installdocs:
	dh_installdocs --doc-main-package=gmsh-doc -pgmsh
	dh_installdocs --doc-main-package=gmsh-doc -pgmsh-doc
	dh_installdocs --doc-main-package=gmsh-doc -plibgmsh-dev
	dh_installdocs --doc-main-package=gmsh-doc -plibgmsh4.8
	dh_installdocs --doc-main-package=gmsh-doc -ppython3-gmsh

override_dh_auto_test:
	mkdir test-dir
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
    ifeq (,$(filter $(DEB_HOST_ARCH),$(disable_auto_test_archs)))
	cp demos/simple_geo/transfinite.geo test-dir
	cd test-dir; LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${BUILDDIR}" $(BUILDDIR)/gmsh transfinite.geo -2 -o transfinite2d.mesh
	cd test-dir; LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${BUILDDIR}" $(BUILDDIR)/gmsh transfinite.geo -3 -o transfinite3d.mesh
	cd test-dir; LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${BUILDDIR}" $(BUILDDIR)/gmsh transfinite.geo -2 -o transfinite2d.msh
	cd test-dir; LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${BUILDDIR}" $(BUILDDIR)/gmsh transfinite.geo -3 -o transfinite3d.msh
	cd test-dir; LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${BUILDDIR}" $(BUILDDIR)/gmsh transfinite.geo -2 -o transfinite2d.stl
	cd test-dir; LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${BUILDDIR}" $(BUILDDIR)/gmsh transfinite.geo -3 -o transfinite3d.stl
	cd test-dir; ls -ln;
    endif
endif
	rm -rf test-dir
